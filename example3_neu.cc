#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

int main()
{
  int i = 0;
  string dateipfad = "output.txt";
  ifstream datei(dateipfad);
  // Überprüft ob Datei geöffnet
  if (!datei.is_open())
  {
    cerr << "Die Datei konnte nicht geöffnet werden." << endl;
    return 1;
  }

  string zeile;
  while (getline(datei, zeile))
  {
    i++;

    // Eingabestrom für die Zeile erstellen
    istringstream zeilenstrom(zeile);

    // Zahlen extrahieren
    string zahl1, zahl2, zahl3, zahl4, zahl5;

    // Ignoriere die ersten beiden Zahlen
    getline(zeilenstrom, zahl1, ',');
    getline(zeilenstrom, zahl2, ',');

    // Lese die gewünschten Zahlen
    getline(zeilenstrom, zahl3, ',');
    getline(zeilenstrom, zahl4, ',');
    getline(zeilenstrom, zahl5, ',');

    // Ausgabe der Ergebnisse
    // cout << "Die 3., 4. und 5. Zahl sind: " << zahl3 << ", " << zahl4 << ", " << zahl5 << endl;

    string befehl = "./myprogram_neu " + zahl3 + " " + zahl4 + " " + zahl5 + " " + "0.693" + " " + ">file_neu.dat";
    system(befehl.c_str());

    string dateipfad2="file_neu.dat";
    ifstream datei2(dateipfad2);
    // Überprüft ob Datei geöffnet
    if (!datei2.is_open())
   {
    cerr << "Die Datei konnte nicht geöffnet werden." << endl;
    return 1;
   }
   string ergebnis1, minimum, zeile2;
   getline(datei2, zeile2);
  

    // Eingabestrom für die Zeile erstellen
    istringstream zeilenstrom2(zeile2);
    getline(zeilenstrom2, ergebnis1,' ');
    getline(zeilenstrom2, minimum, ' ');
   
    
    ofstream outputFile("output_neu.txt", ios::app);

    if (outputFile.is_open())
    {
      outputFile << zeile << "," << minimum << "\n";
      outputFile.close();

      cout << i <<"\n";
    }
    else
    {
      cerr << "Fehler beim Öffnen der Ausgabedatei im Anhangmodus.\n";
    }
    if (i == 950)
    {
      break;
    }
  }

  // Datei schließen
  datei.close();

  return 0;
}