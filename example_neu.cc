#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <vector>

using namespace std;

// Some constants useful in solar system
#define mmoon 7.342e25           // Moon mass in g
#define mearth 5.97219e27        // Earth mass in g 
#define msun 1.989e33            // Sun mass in g 
#define au 1.495978707e13        // au in cm 
#define rearth 6371e5            // Earth dameter in cm 
#define dmoon 384400e5           // Distance moon in cm 
#define sec_per_year 3.1556926e7 // seconds per year 
#define gyear 3.1556926e16       // seconds per Gyear 
#define parsec 3.085678e18       // cm per parsec
#define kparsec 3.085678e21      // cm per kilo parsec
#define mparsec 3.085678e24      // cm per mega parsec
#define km_per_sec 1e5           // km per sec

// unit system of the simulation (galaxies)
#define m_unit 1e10*msun
#define l_unit kparsec
#define v_unit km_per_sec        // km/s
#define t_unit (l_unit / v_unit) 

// Gravitational constant and c in internal units 
#define G 6.672e-8 * m_unit * t_unit * t_unit / l_unit / l_unit / l_unit
#define c 2.99792458e+10 * t_unit / l_unit
#define OMEGA_PLANCK 0.693                   // Cosmological Constant  
#define h 67.77                              // Hubble parameter  
#define H0 (h * km_per_sec / mparsec / v_unit * l_unit)     // Hubble constant in internal units
#define T0 13.799         // Age of the Universe  

/* here we define the variables of the system which
   we want to keep for each time within one structure */

struct state
{
  double r,v,a;
  double m;

  state() { r=v=a=m=0; }
  state(double _r,double _v,double _m) { a=0; r=_r; v=_v; m=_m;}
};


/* Here are our Local Group values */

double VRAD;  // -120.0            radial velocity
double VTANG; //   17.0,57.0,82.5  tangential velocity
double R0;    //  770.0            Distance of MW and Andromeda
double OMEGA_LAMBDA = OMEGA_PLANCK;

// Setup a grid of masses to test 
void initialize_system(vector<state> &start)
{
  // setup a grid of masses
  for(double mass=100;mass<5500;mass+=1)
    start.push_back(state(R0,VRAD,mass));
}

// Calculate acceleration of a given state. This is a procedure which gets a state as an argument
void calculate_acceleration(vector<state> &here, double t)
{
  double l = R0 * kparsec / l_unit * VTANG * km_per_sec / v_unit;
  //  double HUBBLE = OMEGA_LAMBDA * H0 * (T0 * gyear / t_unit) / t;
  double HUBBLE = H0;
#pragma omp parallel for
  for(int i=0;i<here.size();i++)
    {
      double r = here[i].r;
      double m = here[i].m;
      here[i].a = (l*l)/(r*r*r) - G*m/(r*r) + OMEGA_LAMBDA*HUBBLE*HUBBLE*r;
    }
}

// update velocities of a given state. This is a procedure which gets a state and a deltaT as arguments
void kick(vector<state> &here, double mydt)
{
#pragma omp parallel for
  for(int i=0;i<here.size();i++)
    here[i].v += here[i].a * mydt;
}

// update positions of a given state. This is a procedure which gets a state and a deltaT as argument
void drift(vector<state> &here, double mydt)
{
#pragma omp parallel for
  for(int i=0;i<here.size();i++)
    {
      here[i].r += here[i].v * mydt;
      if(here[i].r < 0)
	{
	  here[i].r *= -1;
	  here[i].v *= -1;
	}
    }
}


int main (int argc, const char **argv)
{
  /* state is a structure (we defined it in example.h) which contains all quantities (like position, velocity etc)
     needed to describe a test particle.
     We want to create one instance of it which we call current for or actual time step */
  
  vector<state> current;          // Vector of our particle system
  double t = T0 * gyear / t_unit; // Time of the system
  double dt = -0.0001;            // Timestep to be defined later

  if(argc<4)
    {
      cout << "please give parameters:" << endl << "./myprogram dist vrad vtan [LAMBDA]" << endl;
      for(int i=0;i<argc;i++)
	cout << i << ":" << argv[i] << endl;
      return(0);
    }
  R0 = atof(argv[1]);
  VRAD = atof(argv[2]);
  VTANG = atof(argv[3]);
  if(argc>4)
    OMEGA_LAMBDA = atof(argv[4]);
  
  initialize_system(current);  
  
  // Lets output at the beginning some useful information ... 
  //cout << "# [L]    = " << l_unit << endl;
  /*cout << "# [T]    = " << t_unit << endl;
  cout << "# [M]    = " << m_unit << endl;
  cout << "# [V]    = " << v_unit << endl;
  cout << "# G      = " << G << " in internal units " << endl;
  cout << "# c      = " << c << " in internal units " << endl;
  cout << "# h      = " << h << " km/sec/mparsck " << endl;
  cout << "# OmegaL = " << OMEGA_LAMBDA << " unitless " << endl;
  cout << "# Age    = " << T0 << " Gyears " << endl;
  cout << "# Dist   = " << R0 << " kparsck " << endl;
  cout << "# vtan   = " << VTANG << " km/sec " << endl;
  cout << "# vrad   = " << VRAD << " km/sec " << endl;*/

  // Now we calculate the acceleration of the initial statey
  calculate_acceleration(current,t);

  // This is loop which is done until integration of t reaches our defined TMAX
  while(t > 0)
    {
      // KDK Scheme
      kick(current, 0.5 * dt);
      drift(current, 1.0 * dt);
      calculate_acceleration(current,t+dt);
      kick(current, 0.5 * dt);

      // Finally we update the time t
      t += dt;
    }
  //cout << "# dist(t=0) mass" << endl;

  double last =  current[0].r;
  double mass = 0;
  int found = 0;
  
  for(int i=0; i< current.size(); i++)
    {
      if ((current[i].r > last) && (found == 0) && (i>0))
	{
	  last = current[i-1].r;
	  mass = current[i-1].m;
	  found = 1;
	}
      if (found == 0)
	last =  current[i].r;
      
      //cout << current[i].r << " " << current[i].m << " " << endl;
    }
  cout << last << " " << mass << endl;
  return(0);
}


/* How to get it running (Step 0 has only to be done once):
  0) export OMP_NUM_THREADS=4
  1) g++ example_neu.cc -O3 -fopenmp  -o myprogram_neu
  2) ./myprogram_neu dist vrad vtan [LAMBDA] > file_neu.dat
*/
